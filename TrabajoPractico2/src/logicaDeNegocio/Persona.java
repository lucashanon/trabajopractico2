package logicaDeNegocio;

public class Persona {
	public String nombre;
	public int interesDeporte;
	private int interesMusica;
	private int interesEspectaculo;
	private int interesCiencia;

	public Persona(String nombre, int deporte, int musica, int espectaculo, int ciencia) {
		this.nombre=nombre;
		interesDeporte=deporte;
		interesMusica=musica;
		interesEspectaculo=espectaculo;
		interesCiencia=ciencia;
	}
	
	//similaridad(i, j) = |di − dj | + |mi − mj | + |ei − ej | + |ci − cj |.
	int Similaridad(Persona p2) {
		return (Math.abs(interesDeporte-p2.getInteresDeporte())+
		Math.abs(interesMusica-p2.getInteresMusica())+
		Math.abs(interesEspectaculo-p2.getInteresEspectaculo())+
		Math.abs(interesCiencia-p2.getInteresCiencia()));
	}

	public String getNombre() {
		return nombre;
	}

	int getInteresDeporte() {
		return interesDeporte;
	}

	int getInteresMusica() {
		return interesMusica;
	}

	int getInteresEspectaculo() {
		return interesEspectaculo;
	}

	int getInteresCiencia() {
		return interesCiencia;
	}

	

}
