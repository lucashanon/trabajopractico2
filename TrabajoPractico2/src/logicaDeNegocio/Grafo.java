package logicaDeNegocio;

import java.util.HashSet;
import java.util.Set;

public class Grafo {
	private int[][] Grafo;
	
	public Grafo(int vertices) {
		Grafo=new int[vertices][vertices];
	}
	
	public void agregarArista(Persona p1, Persona p2, int indiceP1, int indiceP2){
	
		verificarVertice(indiceP1);
		verificarVertice(indiceP2);

		if(indiceP1==indiceP2) {
			Grafo[indiceP1][indiceP2] = 0;
			Grafo[indiceP2][indiceP1] = 0;
		}else {
			int similaridad=p1.Similaridad(p2);
			Grafo[indiceP1][indiceP2] = similaridad;
			Grafo[indiceP2][indiceP1] = similaridad;
		}

	
	}
	
	public void generadorMinimo(Integer indiceP1, Integer indiceP2,Integer similaridad) {
		Grafo[indiceP1][indiceP2] = similaridad;
		Grafo[indiceP2][indiceP1] = similaridad;
	}
	
	// Vecinos de un vertice
	public Set<Integer> vecinos(Integer i)
	{	
		Set<Integer> ret = new HashSet<Integer>();
		for(Integer j = 0; j < Grafo.length; ++j) 
			 if( i != j )
				{
					if( this.existeArista(i,j) )
						ret.add(j);
				}
				
				return ret;		
			}
			


	public boolean existeArista(int i, int j)
	
	{
		return Grafo[i][j]!=0;
	}

	public void eliminarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);

		Grafo[i][j] = 0;
		Grafo[j][i] = 0;
	}

	
	public int tamanio()
	{
		return Grafo.length;
	}

	// Verifica que sea un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= Grafo.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	
	public int retornarArista(int x,int y) {
		return Grafo[x][y];
	}

}
