package logicaDeNegocio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

public class GrafoTest {

	@Test
	public void todosAisladosTest()
	{
		Grafo grafo = new Grafo(5);
		assertEquals(0, grafo.vecinos(2).size());
	}
	

	@Test(expected = IllegalArgumentException.class)
	public void primerVerticeNegativoTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,-1, 3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void primerVerticeExcedidoTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,5, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeNegativoTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,2, -1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeExcedidoTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,2, 5);
	}

	@Test
	public void aristaExistenteTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,2, 3);
		assertTrue( grafo.existeArista(2, 3) );
	}

	@Test
	public void aristaOpuestaTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,2, 3);
		assertTrue( grafo.existeArista(3, 2) );
	}

	@Test
	public void aristaInexistenteTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,2, 3);
		assertFalse( grafo.existeArista(1, 4) );
	}

	@Test
	public void agregarAristaDosVecesTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,2, 3);
		grafo.agregarArista(p1, p2,2, 3);

		assertTrue( grafo.existeArista(2, 3) );
	}
	
	@Test
	public void eliminarAristaExistenteTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,2, 4);
		
		grafo.eliminarArista(2, 4);
		assertFalse( grafo.existeArista(2, 4) );
	}

	@Test
	public void eliminarAristaInexistenteTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 4);
		assertFalse( grafo.existeArista(2, 4) );
	}
	
	@Test
	public void eliminarAristaDosVecesTest()
	{
		Persona p1 = new Persona("Lucas",1,2,3,4);
		Persona p2 = new Persona("Kiara",2,3,4,5);
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(p1, p2,2, 4);
		
		grafo.eliminarArista(2, 4);
		grafo.eliminarArista(2, 4);
		assertFalse( grafo.existeArista(2, 4) );
	}


}
