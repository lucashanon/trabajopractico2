package logicaDeNegocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonaTest {

	
	@Test(expected =java.lang.StackOverflowError.class)
	public void PersonaInvalida() {
		Persona p1 = new Persona("Lucas",20,20,30,40);
		Logica log=new Logica();
		log.CargarPersona(p1);
	}
	
	@Test(expected =java.lang.StackOverflowError.class)
	public void PersonaInvalidaDeporte() {
		Persona p1 = new Persona("Lucas",20,2,3,4);
		Logica log=new Logica();
		log.CargarPersona(p1);
	}
	
	@Test(expected =java.lang.StackOverflowError.class)
	public void PersonaInvalidaMusica() {
		Persona p1 = new Persona("Lucas",2,20,3,4);
		Logica log=new Logica();
		log.CargarPersona(p1);
	}
	
	@Test(expected =java.lang.StackOverflowError.class)
	public void PersonaInvalidaEspectaculo() {
		Persona p1 = new Persona("Lucas",2,2,30,4);
		Logica log=new Logica();
		log.CargarPersona(p1);
	}
	
	@Test(expected =java.lang.StackOverflowError.class)
	public void PersonaInvalidaCiencia() {
		Persona p1 = new Persona("Lucas",2,2,3,40);
		Logica log=new Logica();
		log.CargarPersona(p1);
	}


}


