package logicaDeNegocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mvp.Presenter;
public class Logica {
	public static List<Persona> listaDePersonas = new ArrayList<>();
	public static Grafo Grafo_G;
	public static Grafo Arbol;

	public Logica() {
	    listaDePersonas = new ArrayList<>();
	    Grafo_G = null;
	    Arbol = null;
	}

	
	public static void CargarPersona(Persona p) {
			listaDePersonas.add(p);
	}
	
	public static void crearGrafo() {
		Grafo_G=new Grafo(listaDePersonas.size());
		agregarConexiones();
	}
	
	public static void agregarConexiones() {
		for (int i=0;i<listaDePersonas.size();i++) {
			for (int x=0;x<listaDePersonas.size();x++) {
				Grafo_G.agregarArista(listaDePersonas.get(i), listaDePersonas.get(x), i, x);
			}
		}
	}
	
	public static Grafo mostrarGrafo() {
		return Grafo_G;
	}
	
	public static Grafo ArbolGeneradorMinimo() {
		
		List<Integer> VerticeVisitado = new ArrayList<>();
		VerticeVisitado.add(0);
		Integer vecino = null;
		Integer vertice = null;
		Integer actual = null;
		Arbol=new Grafo(Grafo_G.tamanio());
		while(VerticeVisitado.size()<Grafo_G.tamanio()) {
			Integer arista=Integer.MAX_VALUE;
			for (Integer x : VerticeVisitado) {
				vecino=aristaMinima(x,VerticeVisitado);
				if(vecino!=null && Grafo_G.retornarArista(x, vecino)<arista) {
					arista=Grafo_G.retornarArista(x, vecino);
					vertice=vecino;
					actual=x;					
				}
			}
			VerticeVisitado.add(vertice);
			Arbol.generadorMinimo(actual, vertice, Grafo_G.retornarArista(actual, vertice));
		}
		return Arbol;
	}
		
	public static Integer aristaMinima(Integer i, List<Integer> visitados){
		Set<Integer> ret = new HashSet<Integer>();
		ret=Grafo_G.vecinos(i);
		Integer vecino = null;
		Integer min = Integer.MAX_VALUE;
		for(Integer i1 : ret ) {
	        if (min > Grafo_G.retornarArista(i, i1) && !visitados.contains(i1)) {
	            min = Grafo_G.retornarArista(i, i1);
	            vecino=i1;
	        }
	    }	 
	    return vecino;
	}
	
	public static void dividirGrafo(){
		int max= Integer.MIN_VALUE;
		int valor1=0;
		int valor2=0;
		for(int x=0;x<Arbol.tamanio();x++) {
			for(int y=0;y<Arbol.tamanio();y++) {
				if(max<Arbol.retornarArista(x, y)) {
					max=Arbol.retornarArista(x, y);
					valor1=x;
					valor2=y;
				}
			}
		}
		Arbol.eliminarArista(valor1, valor2);
	}

	public static int tamano()
	{
		return listaDePersonas.size();
	}
	
	public static List<List<Integer>> imprimirComponentesConexas() {
		System.out.println("Imprimiendo componentes conexas:");
	    boolean[] visitado = new boolean[Arbol.tamanio()];
	    List<List<Integer>> componentesConexas = new ArrayList<>();

	    for (int i = 0; i < Arbol.tamanio(); i++) {
	        if (!visitado[i]) {
	            List<Integer> componente = new ArrayList<>();
	            dfs(i, visitado, componente);
	            componentesConexas.add(componente);
	        }
	    }

	    // Imprimir las componentes conexas en forma de lista
//	    for (int i = 0; i < componentesConexas.size(); i++) {
//	        System.out.println("Componente " + (i + 1) + ": " + componentesConexas.get(i));
//	    }
	    return componentesConexas;
	}


	private static void dfs(int vertice, boolean[] visitado, List<Integer> componente) {
	    visitado[vertice] = true;
	    componente.add(vertice);

	    Set<Integer> vecinos = Grafo_G.vecinos(vertice);
	    for (int vecino : vecinos) {
	        if (!visitado[vecino]) {
	            dfs(vecino, visitado, componente);
	        }
	    }
	}

	private static void imprimirConexas(List<List<Integer>> componentesConexas) {
	    for (int i = 0; i < componentesConexas.size(); i++) {
	        System.out.println("Componente " + (i + 1) + ": " + componentesConexas.get(i));
	    }
	}
	
}
