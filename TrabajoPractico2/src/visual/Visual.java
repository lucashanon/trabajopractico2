package visual;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import logicaDeNegocio.Logica;
import logicaDeNegocio.Persona;
import mvp.Presenter;
import logicaDeNegocio.Grafo;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JSeparator;


public class Visual  {

	private JFrame frame;
	JTextField textNombre;
	JComboBox<String> BoxInteresDeporte ;
	JComboBox<String> BoxInteresMusica;
	JComboBox<String> BoxInteresEspectaculo ;
	JComboBox<String> BoxInteresCiencia;
	JButton btnVerResultados;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Visual window = new Visual();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
			
	
	protected void limpiar() {
		textNombre.setText("");
		BoxInteresDeporte.setModel(new DefaultComboBoxModel<String>(new String[] {"1", "2", "3", "4", "5"}));
		BoxInteresMusica.setModel(new DefaultComboBoxModel<String>(new String[] {"1", "2", "3", "4", "5"}));
		BoxInteresEspectaculo.setModel(new DefaultComboBoxModel<String>(new String[] {"1", "2", "3", "4", "5"})); 
		BoxInteresCiencia.setModel(new DefaultComboBoxModel<String>(new String[] {"1", "2", "3", "4", "5"}));
 
}
	

	
	/**
	 * Create the application.
	 */
	public Visual() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(240, 255, 240));
		frame.setBounds(100, 100, 547, 522);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textNombre = new JTextField();
		textNombre.setBounds(277, 53, 231, 20);
		frame.getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		JLabel LabelNombre = new JLabel("Nombre:");
		LabelNombre.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		LabelNombre.setBounds(27, 52, 54, 20);
		frame.getContentPane().add(LabelNombre);
		
		JLabel LabelDeporte = new JLabel("Interes por los deportes");
		LabelDeporte.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		LabelDeporte.setBounds(27, 131, 163, 20);
		frame.getContentPane().add(LabelDeporte);
		
		JLabel LabelMusica = new JLabel("Interes por la musica");
		LabelMusica.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		LabelMusica.setBounds(27, 196, 163, 20);
		frame.getContentPane().add(LabelMusica);
		
		JLabel LabelEspectaculo = new JLabel("Interes por las noticias del espectaculo");
		LabelEspectaculo.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		LabelEspectaculo.setBounds(27, 258, 240, 25);
		frame.getContentPane().add(LabelEspectaculo);
		
		JLabel LabelCiencia = new JLabel("Interes por la ciencia");
		LabelCiencia.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		LabelCiencia.setBounds(27, 324, 163, 20);
		frame.getContentPane().add(LabelCiencia);
		
		
		BoxInteresDeporte = new JComboBox<String>();
		BoxInteresDeporte.setBackground(new Color(255, 255, 255));
		//BoxInteresDeporte.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		BoxInteresDeporte.setMaximumRowCount(5);
		BoxInteresDeporte.setBounds(324, 129, 37, 27);
		frame.getContentPane().add(BoxInteresDeporte);
		
		BoxInteresMusica = new JComboBox<String>();
		BoxInteresMusica.setBackground(new Color(255, 255, 255));
		//BoxInteresMusica.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		BoxInteresMusica.setMaximumRowCount(5);
		BoxInteresMusica.setBounds(324, 194, 37, 27);
		frame.getContentPane().add(BoxInteresMusica);
		
		BoxInteresEspectaculo = new JComboBox<String>();
		BoxInteresEspectaculo.setBackground(new Color(255, 255, 255));
		//BoxInteresEspectaculo.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		BoxInteresEspectaculo.setMaximumRowCount(5);
		BoxInteresEspectaculo.setBounds(324, 256, 37, 27);
		frame.getContentPane().add(BoxInteresEspectaculo);
		
		BoxInteresCiencia = new JComboBox<String>();
		BoxInteresCiencia.setBackground(new Color(255, 255, 255));
		//BoxInteresCiencia.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		BoxInteresCiencia.setMaximumRowCount(5);
		BoxInteresCiencia.setBounds(324, 322, 37, 27);
		frame.getContentPane().add(BoxInteresCiencia);
		
		limpiar();

		JButton ButtonAgregarlista = new JButton("Agregar a la lista");
		ButtonAgregarlista.setBackground(new Color(255, 255, 255));
		ButtonAgregarlista.setIcon(null);
		ButtonAgregarlista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Presenter.cargarPersona(textNombre.getText(),
						Integer.parseInt(BoxInteresDeporte.getSelectedItem().toString()), 
						Integer.parseInt(BoxInteresMusica.getSelectedItem().toString()),
						Integer.parseInt(BoxInteresEspectaculo.getSelectedItem().toString()),
						Integer.parseInt(BoxInteresCiencia.getSelectedItem().toString()));
				limpiar();
			}});
		ButtonAgregarlista.setBounds(41, 432, 163, 23);
		frame.getContentPane().add(ButtonAgregarlista);		
		btnVerResultados = new JButton("Ver Resultados");
		btnVerResultados.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
				
				try {
						
					if(Presenter.tamaño()<2){
						JOptionPane.showMessageDialog(null, "tiene que haber mas de dos personas ");
					}
					else{
					Presenter.CrearRespuesta();
		
					Resultados.main(null);						
					frame.dispose();
					;}
					
					
				} catch (Exception e1) {JOptionPane.showMessageDialog(null, "ERROR");}		
			
			}	
		});
		
		btnVerResultados.setBounds(274, 432, 163, 23);
		frame.getContentPane().add(btnVerResultados);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(173, 216, 230));
		panel.setBounds(0, 0, 267, 421);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(175, 238, 238));
		panel_1.setBounds(-19, 422, 562, 61);
		frame.getContentPane().add(panel_1);
	
	
	
	}
	
	


}










