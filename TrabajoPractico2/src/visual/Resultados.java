package visual;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import logicaDeNegocio.Logica;
import logicaDeNegocio.Persona;
import mvp.Presenter;
import logicaDeNegocio.Grafo;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class Resultados {

	private JFrame frame;
	private JTable table;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Resultados window = new Resultados();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public void repuesta() {
		Grafo g=Presenter.MostrarRespuesta();
		String cadena="el resultado es :";
		cadena+="\n";
		for (int i = 0; i < Logica.Arbol.tamanio(); i++) {
		    for (int j = 0; j < Logica.Arbol.tamanio(); j++) {
		      cadena+=Logica.Arbol.retornarArista(i, j); 
		      cadena+="";
		    }
		    cadena+="\n";
		} 
		textArea.setText(cadena);
	}
		

	public void repuesta2() {
		Grafo g=Presenter.MostrarRespuesta();
		String cadena="el resultado es :";
		cadena+="\n";
		for (int i = 0; i < Logica.Arbol.tamanio(); i++) {
		    for (int j = 0; j < Logica.Arbol.tamanio(); j++) {
		    	if (Logica.Arbol.existeArista(i, i)) {
		    		cadena+=Logica.Arbol.retornarArista(i, j); 
		    		cadena+="-";
		    		cadena+="-";
		    		cadena+="-";
		    		cadena+="-";
		    		cadena+="-";
		    		cadena+="-";
		    }
		    }
		    cadena+="\n";
		} 
		textArea.setText(cadena);
	}

	
	/**
	 * Create the application.
	 */
	public Resultados() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 953, 295);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(135, 206, 250));
		panel.setBounds(0, 0, 937, 55);
		frame.getContentPane().add(panel);
		
		JLabel lblNewLabel = new JLabel("RESULTADOS");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 39));
		panel.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 55, 937, 201);
		frame.getContentPane().add(scrollPane);
		
	    textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 15));
		scrollPane.setViewportView(textArea);
	
		repuesta();
	
	}
}
