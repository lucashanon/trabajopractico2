package mvp;

import logicaDeNegocio.*;
import visual.Visual;


public class Presenter {

	public static void cargarPersona(String nombre, int deporte, int musica, int espectaculo, int ciencia) {
		Persona persona = new Persona(nombre,deporte,musica,espectaculo,ciencia);
		Logica.CargarPersona(persona);
	}

	public static int tamaño() {
		return Logica.tamano();
				
	}
	
	public static void armargrafo() {
		Logica.crearGrafo();
		
	}
	
	public static void ResultadosArbol(){
		 Logica.ArbolGeneradorMinimo();
		
	}
	
	public static void CrearRespuesta() {
		Logica.crearGrafo();
//		for (int i = 0; i < Logica.Grafo_G.tamanio(); i++) {
//		    for (int j = 0; j < Logica.Grafo_G.tamanio(); j++) {
//		        System.out.print(Logica.Grafo_G.retornarArista(i, j) + " ");
//		    }
//		    System.out.println();
//		}
//		System.out.println();
		Logica.ArbolGeneradorMinimo();
//		for (int i = 0; i < Logica.Arbol.tamanio(); i++) {
//		    for (int j = 0; j < Logica.Arbol.tamanio(); j++) {
//		        System.out.print(Logica.Arbol.retornarArista(i, j) + " ");
//		    }
//		    System.out.println();
//		}
//		System.out.println();
		Logica.dividirGrafo();
//		for (int i = 0; i < Logica.Arbol.tamanio(); i++) {
//		    for (int j = 0; j < Logica.Arbol.tamanio(); j++) {
//		        System.out.print(Logica.Arbol.retornarArista(i, j) + " ");
//		    }
//		    System.out.println();
//		}
	}
	
	public static Grafo MostrarRespuesta() {
		return Logica.Arbol;
	}
	
	public static Grafo MostrarGrafo() {
		return Logica.mostrarGrafo();
	}
	
}